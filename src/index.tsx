import React from "react";
import { render } from "react-dom";

import App from "./AppMPStarter/application/App";

render(<App />, document.getElementById("root"));
