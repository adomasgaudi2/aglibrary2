import React from "react";
import { Box } from "@mui/material";
import Header from "../application/layout/Header/Header";

export const BlogCont = ({ children }: any) => {
  return (
    <>
      <Header />
      <Box mx="auto" width="85%" maxWidth="800px" pb="20rem">
        {children}
      </Box>
    </>
  );
};
export default BlogCont;
