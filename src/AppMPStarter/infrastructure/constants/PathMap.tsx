import React from "react";
import { lazy } from "react";

const Home = lazy(() => import("../../application/pages/Home"));
const Sports = lazy(() => import("../../application/pages/Blogs/Sports"));
const Chess = lazy(() => import("../../application/pages/Blogs/Chess"));
const Ordinary = lazy(() => import("../../application/pages/Blogs/Ordinary"));
const Art = lazy(() => import("../../application/pages/Blogs/Art"));
const DeepLearning = lazy(
  () => import("../../application/pages/Blogs/DeepLearning")
);
const Food = lazy(() => import("../../application/pages/Blogs/Food"));
const FactoryReset = lazy(
  () => import("../../application/pages/Blogs/FactoryReset")
);

const RoutesMap = [
  { name: "", component: <Home /> },
  { name: "sports", component: <Sports /> },
  { name: "chess", component: <Chess /> },
  { name: "ordinary", component: <Ordinary /> },
  { name: "art", component: <Art /> },
  { name: "deepLearning", component: <DeepLearning /> },
  { name: "food", component: <Food /> },
  { name: "factoryReset", component: <FactoryReset /> },
];

export interface routeType {
  name: string;
  component: React.ReactNode;
}

export default RoutesMap;
