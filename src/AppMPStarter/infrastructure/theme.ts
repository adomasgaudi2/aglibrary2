import { createTheme } from "@mui/material";
import { grey } from "@mui/material/colors";

const theme = createTheme({
  palette: {
    background: {
      default: grey.A100,
    },
  },
  typography: {
    fontFamily: "Open Sans",
    h1: {
      fontFamily: "Fira Code",
      fontSize: 48,
      fontWeight: 700,
    },
    h2: {
      fontFamily: "Fira code",
      fontSize: 38,
      fontWeight: 700,
    },
    h3: {
      fontFamily: "Fira code",
      fontSize: 24,
      fontWeight: 700,
    },
    h4: {
      fontFamily: "Fira code",
      fontSize: 20,
      fontWeight: 700,
    },
    h5: {
      fontFamily: "Fira code",
      fontSize: 18,
      fontWeight: 700,
    },
    h6: {
      fontFamily: "Fira code",
      fontSize: 16,
      fontWeight: 700,
    },
    subtitle1: {
      fontWeight: 500,
      fontFamily: "Fira code",
      fontSize: 18,
    },
    subtitle2: {
      fontWeight: 500,
      fontFamily: "Fira code",
      fontSize: 16,
    },
    body1: {
      fontFamily: "Fira code",
      margin: "0 0 2rem 0",
      fontSize: 16,
      fontWeight: 400,
    },
    body2: {
      fontFamily: "Fira code",
      fontSize: 14,
      fontWeight: 400,
    },
    button: {
      textTransform: "none",
      fontSize: 16,
      fontFamily: "Fira code",
      fontWeight: 500,
    },
  },
});

export default theme;
