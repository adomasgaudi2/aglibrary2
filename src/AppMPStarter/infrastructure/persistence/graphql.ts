import { ApolloClient, InMemoryCache } from "@apollo/client";

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  uri: "https://regular-sponge-15.hasura.app/v1/graphql",
  headers: {
    "x-hasura-admin-secret":
      "qzL5JRoxMj29S31Fh8ov31ZWYPLXclyJvKU0VjfHcZL7DuoI3BTHw8acvjXCW8GT",
  },
});

export default apolloClient;
