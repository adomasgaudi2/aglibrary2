import { gql, QueryHookOptions, useQuery } from '@apollo/client';



const GET_CURRENT_USER = gql`
  query userProfile {
    userProfile {
      id
      firstName
      lastName
      email
    }
  }
`;

export default (options?: QueryHookOptions) => useQuery(GET_CURRENT_USER, options);
