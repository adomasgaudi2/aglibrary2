import React from "react";
import { Box, Typography } from "@mui/material";
import BlogCont from "../../../ui/BlogCont";
import Header from "../../layout/Header/Header";

export const Chess = () => {
  return (
    <BlogCont>
      <Box mt="1rem" sx={{ display: "flex" }}>
        <Box>
          <Typography variant="h4">ART</Typography>
          <Typography variant="body1">
            Collections of top artists of CC age
            <ul>
              <li>bisquiat 1950-1980</li>
              <li>KEITH HARING</li>
              <li>CHRISTOPHER WOOL</li>
            </ul>
          </Typography>
        </Box>
      </Box>
    </BlogCont>
  );
};
export default Chess;
