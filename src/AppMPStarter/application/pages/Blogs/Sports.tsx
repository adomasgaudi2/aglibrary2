import React from "react";
import { Box, Typography } from "@mui/material";
import BlogCont from "../../../ui/BlogCont";
import Title from "../../../comps/Title/Title";

export const Sports = () => {
  return (
    <BlogCont>
      <Box mt="1rem">
        <Title>Sport</Title>
        <Typography variant="h4">How to exercise</Typography>
        <Box my="2rem">
          <hr />
        </Box>
        <Typography variant="h5">
          Why you should become a bodybuilder for a year
        </Typography>
        <Typography variant="body1">
          You might think that it's not what you want, but just entertain this
          idea for a second. First of all, basically no one becomes super bulk
          in a year, it takes years, plural. Bulking for a year is not gonna
          make you worse of whatever your goals are. And I sincerely mean that
          for women as well, being a guy.
        </Typography>
        <Typography variant="body1">
          Given that, let's see what exactly would be
        </Typography>
        <Box my="2rem">
          <hr />
        </Box>
        <Typography variant="h4">
          the benefit of bodybuilding for a year.
        </Typography>
        <Typography variant="body1">
          The biggest obsticle to any king of exercising is simply doing it.
          Doing it consistently and creating a habit out of it. Quite possibly
          the most important issue of exercising in my opinion is not adressed
          enough. So let me give you some pointers.
        </Typography>
        <Typography variant="body1">
          It seems quite common to find young guys that just can't stop going to
          the gym and they keep talking about how they feel addicted to the
          "burn". Now, "generic musclehead" and "discipline" do not associate in
          my head that well, so I think it's worth considering this as an
          example that that has the specific environment to successfully
          habituate this unhabituable task.
        </Typography>
        <Box my="2rem">
          <hr />
        </Box>
        <Typography variant="h4">
          Reasons why bodybuilders create good exercising habits.
        </Typography>
        <Typography variant="body1">
          Reason one. Although exercising different muscles seems is equally
          good and indiscriminantly suggested, you might have found that some
          muscles are much more exhausting to train than others. There are
          atleast two reasons that could explain this. First, some muscles are
          not trained that often, making the exercise unclear, difficult, and
          the route to max reps a never ending torture.
        </Typography>
        <Typography variant="body1">
          "Muscleheads" usually focus on large muscle groups and repeat familiar
          exercises. Check.
        </Typography>
        <Typography variant="body1">
          Reason two. Some muscles require larger rep ranges. Usefull exercise
          volume is when you get close to your max ability, your max reps,
          however depending on the resistance of the exercise that could be
          anywhere from 1 to infinite reps (walking), or more realistically from
          3 to 100 reps. Unsurprisingly, reaching that max rep of 80reps is a
          lot more difficult than say doing a 5rep max.
        </Typography>
        <Typography variant="body1">
          "Muscleheads" usually do hight resistance/weight exercises, thus
          reaching max reps in say 5 to 20 reps. Check.
        </Typography>
        <Typography variant="body1">
          Reason three. A lot of young guys are desperate to improve their
          phisique. Thus they will most definetely not miss the opportunity to
          eat 2g/kg of protein per day or even use other suplements. By doing
          this they're not only improving their muscle growth, but
          consequentially, this also rewards them with the dopamine to keep
          going back to the gym. Additionally, their diet is also a constant
          reminder of what they're doing. They're not annoyingly surprised when
          it's time to go the gym. On the contrary, they have been consciously
          eating exactly what they need, be that proteins or carbs, and they
          can't wait to return and see the next days gains.
        </Typography>
        <Typography variant="body1">
          Reason four. Young guys show off frequently. They know their max
          abilities for each exercise and they know their competition. Thats to
          very powerfull psychological tools right there. Social
          comparison/pressure and logging experience and improvements.
        </Typography>
        <Box my="2rem">
          <hr />
        </Box>
        <Typography variant="h4">Diet and exercise.</Typography>
        <Typography variant="body1">
          Why bother. - Everyone values being healthy, but mainly the reason is
          preventative, to not die, not be sick not be disabled or simply
          unhealthy. For most people that is not very motivating as they've
          spent most of their lives and only relate to healthy people. - The
          other reason would be to improve the quality of life, though this
          might seem too elusive again to give any serious consideration.
        </Typography>
        <Typography variant="h4">Diet:</Typography>
        <Typography variant="body1">
          {" "}
          I've tested keto diet, mostly to loose weight and to fell better more
          alert and more clear. Those were my goals. Also my quest to live life
          on hard mode every day so that i can power up like a supersayan also
          plays a role in this - I have never been really out of shape of really
          overweight, but I have always weighed around 82-85kg (after like 18)
          and could never really change that. So I of course was always
          interested in getting some abbs - Keto diet: - The interesting thing
          is that I have basically never been in keto. My keto diet has only
          ever been really keto for only a few days and mostly during my fasts.
          - I have also never been able to fast for more than 4 days. Some
          people say that after the 3rd day you stop beeing hungry and get this
          sense of clarity… Not me. I've only gotten hungrier and drowsier. In
          fact I was fasting once and doing some important programing stuff and
          I had to quit because I couldn't at all focus on my work.
        </Typography>
        <Typography variant="h4">Training::</Typography>
        <Typography variant="body1">
          - What are your goals? Get fit? Do a lot of pushups? Look balanced?
          look buff? - - Firstly, for me, I want to be able to grow muscle. That
          really is the most important thing. Is that in the beginning you
          simply need to be able to build muscle, after that you can do what you
          want with that. - So how to do you build muscle? - Lets take a look at
          the most effective advice in order: - Alocate 2 hours per day to
          exercise. - Eat a lot of protein. - Train close to failure - Do not
          overtrain. - if you feel like you'll be sore the next day, stop.
          Although beneficial it is too dangerous for habit building - Eat some
          carbs before your workout. - Suplement even more protein - Do up to
          6-10 sets per one mucle group, but only a few sessions per week - -
          What about diets - - To feel alert, healthy - keto diet - avoid high
          glycemic foods - these foods promote hunger - create insulin spikes
          that make blood sugar very volatile - avoid high meat, processed meat
          diets - avoid alergens - for me this is a crushing blow as I cannot
          eat eggs or milk. - avoid too much fiber - fiber loosens the stool as
          it is not digestable, - of course some fiber is very benefitial, but
          its easy to find some ultra fibrous recepies in a keto diet such as
          coconut flour or copious amounts of chia puddings. - avoid too much
          salad - avoid too much nuts - have some salad - don't drink when
          eating - avoid high absobtion cafein - avoid drugs - To lose fat -
          avoid vegetable oils - hypocaloric diet - Time restricted fasting or
          just multiple day fasting - Prolonged fasting - trains your ability to
          to eat also creates ketones and lowers insulin which decreases hunger.
          - Keto diet - again ketones supress hunger and low insulin also
          supresses hunger - Keto diet - ketones supress hunger and low insulin
          also supresses hunger - ketones are created from fat, thus using up
          fat resources - low insulin does not create fat - - This fact combined
          with the myriad of benefits of all kinds of fasting and also just in
          general a hypocaloric diet being the most effective tool for weight
          loss.{" "}
        </Typography>
        - - - - -
      </Box>
    </BlogCont>
  );
};
export default Sports;
