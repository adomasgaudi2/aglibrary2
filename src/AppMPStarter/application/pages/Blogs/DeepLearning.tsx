import React from "react";
import { Box, Typography } from "@mui/material";
import BlogCont from "../../../ui/BlogCont";
import Header from "../../layout/Header/Header";

export const Chess = () => {
  return (
    <BlogCont>
      <Box mt="1rem" sx={{ display: "flex" }}>
        <Box>
          <Typography variant="h4">ART</Typography>
          <Typography variant="body1">
            Deep learning
            <ul>
              <li>must use python</li>
              <li>many free tools</li>
              <li>flask to connect to website</li>
            </ul>
          </Typography>
        </Box>
      </Box>
    </BlogCont>
  );
};
export default Chess;
