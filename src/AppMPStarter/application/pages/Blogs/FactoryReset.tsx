import React from "react";
import { Box, Typography } from "@mui/material";
import BlogCont from "../../../ui/BlogCont";
import Title from "../../../comps/Title/Title";

export const Chess = () => {
  return (
    <BlogCont>
      <Box mt="1rem" sx={{ display: "flex" }}>
        <Box>
          <Title>Factory Reset</Title>
          <Typography variant="h4">Steps to refresh everyting</Typography>

          <Typography variant="h2">Install OS</Typography>
          <Typography variant="h2">Install all apps</Typography>
          <Typography variant="body1">
            install:
            <ul>
              <li>VSC </li>
              <li>Git</li>
              <li>NVM / Node.js </li>
              <li>VSC setup</li>
              <li>chrome setup</li>
              <li>get slack</li>
            </ul>
          </Typography>
          <Typography variant="h2">VSC Setup</Typography>
          <Typography variant="body1">
            <ul>
              <li>horizon theme</li>
              <li>eslint, prettier</li>
              <li>tab = 2</li>
              <li>
                download Fira Code, jetBrains, monais?, cascadia?, input mono?
              </li>
              <li>change font family, font weight, font size, zoom</li>
            </ul>
          </Typography>
          <Typography variant="h2">Browser setup</Typography>
          <Typography variant="body1">
            <ul>
              <li>Downlaod chrome, opera</li>
              <li>
                log in to adomas.gaudiesius@mediapark, adomas.gaudi@gmail.com
              </li>
              <li>
                import opera bookmarks, download chrome extensions on opera{" "}
              </li>
              <li>login to bitbucket, github</li>
            </ul>
          </Typography>
        </Box>
      </Box>
    </BlogCont>
  );
};
export default Chess;
