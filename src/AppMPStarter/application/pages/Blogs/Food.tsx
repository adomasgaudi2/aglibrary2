import React from "react";
import { Box, Link, Typography } from "@mui/material";
import BlogCont from "../../../ui/BlogCont";
import Title from "../../../comps/Title/Title";

export const Article = () => {
  // const { iframe } = useStyles()

  return (
    <BlogCont>
      <Box mt="1rem" sx={{ display: "flex", flexFlow: "column nowrap" }}>
        <Title>Food</Title>
        <Typography variant="h4">abstract</Typography>
        <Typography variant="body1">
          Sweeteners
          <ul>
            <li>stevia</li>
            <Link
              href="https://www.mysteviasweet.com.au/au/baking-with-stevia-erythritol-au"
              target="_blank"
            >
              stevia(partial party)
            </Link>
            ------------
            <Link
              href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6049254/"
              target="_blank"
            >
              research paper
            </Link>{" "}
            indicated that reducing sugar by 25% is most optimal, otherwise
            making them less tasty.
            <li>allulose</li>
            <li>erythritol</li>
            <li></li>
          </ul>
        </Typography>
        <Typography variant="h4">cooking</Typography>
        <Typography variant="body1">
          use the oven
          <ul>
            <li>use the over</li>
            <li>
              Don't use the metal sheet, use a ceramic mug or a container from
              heat proof plastic/glass or metal to cook stuff. they are way
              easier to clean.
            </li>
          </ul>
        </Typography>
        <Typography variant="h4">zuchini dessert granola</Typography>
        <Typography variant="body1">
          <Link
            href="https://www.youtube.com/watch?v=lMcSTE5M6Ks&ab_channel=RuledMe"
            target="_blank"
          >
            YT paper
          </Link>
          <iframe
            style={{ width: "100%", maxWidth: "500px" }}
            src="https://www.youtube.com/embed/lMcSTE5M6Ks"
          ></iframe>
          <ul>
            <li>use the over</li>
          </ul>
        </Typography>
      </Box>
    </BlogCont>
  );
};
export default Article;
