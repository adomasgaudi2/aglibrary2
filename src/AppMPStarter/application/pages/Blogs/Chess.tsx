import React from "react";
import { Box, Typography } from "@mui/material";
import BlogCont from "../../../ui/BlogCont";
import { gql, useQuery } from "@apollo/client";
import { MpColumn, MpTable, MpTableType } from "@mp-react/table";
import Title from "../../../comps/Title/Title";

const columns: MpColumn[] = [
  {
    label: "Kings Pawn",
    key: "kings_pawn",
  },
  {
    label: "Queens Pawn",
    key: "queen_pawn",
  },
];

const TableType: MpTableType = {
  columns,
  rowIdKey: "id",
  // selection: 'multiple',
  // totals: true,
  // stickyTotals: true,
  pageSize: 10,
  disablePagination: true,
};

const GET_CHESS = gql`
  query ExampleQuery {
    chess_moves {
      id
      kings_pawn
      queen_pawn
    }
  }
`;

export const Chess = () => {
  const chessquery = useQuery(GET_CHESS);
  const moves = chessquery.data;
  return (
    <BlogCont>
      <Box mt="1rem" sx={{ display: "flex" }}>
        <Box>
          <Title>Chess</Title>
          <Box style={{ width: "300px" }}>
            {moves && (
              <MpTable
                data={moves.chess_moves}
                onGetData={() => null}
                totalsData={moves.chess_moves.length}
                {...TableType}
              />
            )}
          </Box>
          <Typography variant="h4" mt="5rem" mb="2rem">
            Choose an opening
          </Typography>
          <Typography variant="h4" mt="5rem" mb="2rem">
            Theory
          </Typography>
          <Typography variant="h4" mt="5rem" mb="2rem">
            All possible moves
          </Typography>
          <Typography variant="body1">
            There are moves and there are positions. There are two basic moves.
            A take move and a non-take move. Depending on how close a move is to
            a take-move then we could further classify some these moves
            depending on how far away a take move would be. 1 move away from
            take would be an attack move, Two moves away from a take would be a
            threat move and so on . Further take moves we can just call future
            threats as they rarely happen. For example.
          </Typography>
          <Box mt="5rem" mb="2rem">
            <iframe
              src="//www.chess.com/emboard?id=9085459"
              className="chess"
            ></iframe>
          </Box>
          <Typography variant="body1">
            {`In this legendary game known as the "game of the century", young
            Bobby Fischer plays agains a renowened grandmaster Donald Byrne. In
            this game the 5th move with white's black bishop 5.Bf4 is the first
            attack move in this game. The attack is against the enemy b7 pawn.
            Though this attack is passive at the moment as its target is
            protected. This category of attacks would classify as passive as our
            percepted absolute value of this move is negative.`}
          </Typography>
          <br />
          <Typography variant="body1">
            The reply 5. ... d5 is the first active attack. This move threatens
            to take the unprotected pawn on the next move, thus our opponent
            must reply specifically to protect this pawn. As the result of
            loosing that pawn is not a certain victory for Fischer it will be
            classified as a relative attack, since it depends on your view
            whether it will result in negative or positive net gain.
          </Typography>
          <br />
          <Typography variant="body1">
            a real threat since the pawn is protected. a move, but it is an
            attack type of move against the c5 pawn since it can take it on the
            next move. Also it is a threat against the c6 knight since it can
            attack it on the next move.
          </Typography>
          <br />
          <Typography variant="body1">
            Out of the passive moves there is the PIN and DEV or DEF.
          </Typography>
          <br />
          <Typography variant="body1">
            ATK moves: check, garde, fork.
          </Typography>
        </Box>
      </Box>
    </BlogCont>
  );
};
export default Chess;
