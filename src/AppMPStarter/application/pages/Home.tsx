import { Box, Typography } from "@mui/material";
import React from "react";
import Title from "../../comps/Title/Title";
import RoutesMap from "../../infrastructure/constants/PathMap";
import BlogCont from "../../ui/BlogCont";
import Header from "../layout/Header/Header";

export const Home = () => {
  console.log("hi");

  return (
    <BlogCont>
      <Box mt="1rem">
        <Title>All Blogs</Title>
        {RoutesMap.map((route: any) => (
          <Typography key={`${route.name}`} variant="h4">
            <a href={`/${route.name}`}>{route.name}</a>
          </Typography>
        ))}
      </Box>
    </BlogCont>
  );
};
export default Home;
