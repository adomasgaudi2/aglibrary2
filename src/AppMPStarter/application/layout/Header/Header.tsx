import { AppBar, Box, Container, Menu, MenuItem, Toolbar } from "@mui/material";
import React from "react";
import RoutesMap, {
  routeType,
} from "../../../infrastructure/constants/PathMap";
import AppBarLogo from "../../../comps/AppBarLogo";

const Header: React.FC = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  // const menu = () =>

  return (
    <Box>
      <AppBar
        position="static"
        style={{
          background: "mediumAquaMarine",
          boxShadow: "none",
          borderBottom: "1px solid dimgrey",
        }}
      >
        <Container maxWidth="lg">
          <Toolbar
            disableGutters
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <AppBarLogo />
            <Box onClick={handleMenu}>
              <h2>MENU</h2>
            </Box>
            <Menu
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              {RoutesMap.map((route: routeType) => (
                <MenuItem key={`${route.name}`}>
                  {/* <Link to={`/${route.name}`}>{route.name}</Link> */}
                  <a href={`/${route.name}`}>{route.name}</a>
                </MenuItem>
              ))}
            </Menu>
          </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
};

export default Header;
