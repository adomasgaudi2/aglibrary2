import React, { Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RoutesMap from "../../infrastructure/constants/PathMap";

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Suspense fallback={<>loading...</>}>
        <Routes>
          {RoutesMap.map((route: any) => {
            return (
              <Route
                key={route.name}
                path={`/${route.name}`}
                element={route.component}
              />
            );
          })}
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
};

export default Router;
