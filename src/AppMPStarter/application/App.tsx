import React from "react";
import { ApolloProvider } from "@apollo/client";
import { CssBaseline, ThemeProvider } from "@mui/material";
import "../infrastructure/index.css";
import apolloClient from "../infrastructure/persistence/graphql";
import theme from "../infrastructure/theme";
import Router from "./routing/Router";

const App: React.FC = () => {
  return (
    <ApolloProvider client={apolloClient}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router />
      </ThemeProvider>
    </ApolloProvider>
  );
};

export default App;
