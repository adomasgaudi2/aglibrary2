import React from "react";

export const AppBarLogo = () => {
  return (
    <a
      href="/"
      style={{
        color: "black",
        textDecoration: "none",
        border: "1px solid grey",
        padding: "0 1rem",
        margin: "0 2rem 0 0",
        height: "1rem",
      }}
    >
      <h1>Diagon alley</h1>
    </a>
  );
};
export default AppBarLogo;
