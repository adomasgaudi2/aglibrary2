import React from "react";
import { Typography } from "@mui/material";

const Title = ({ children }: any) => {
  return (
    <>
      <Typography variant="h1" mt="5rem" mb="2rem">
        {children}
      </Typography>
    </>
  );
};

export default Title;
